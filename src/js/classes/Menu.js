export default class Menu {
  constructor(ctx, prevBtn, nextBtn) {
    this.ctx = ctx;
    this.x = 0;
    this.width = 0;
    this.height = 0;
    this.visibleX = 0;
    this.visibleY = 0;
    this.elements = [];
    this.lastElementX = 0;
    this.prevButton = null;
    this.nextButton = null;
    this.idNewElement = 0;
    this.prevImage = prevBtn;
    this.nextImage = nextBtn;
    this.gapX = 0;
    this.gapY = 0;
  }
  init(width, height) {
    this.prevButton = new Button(this.visibleX, this.visibleY, height / 2, height);
    this.nextButton = new Button(width - height / 2, this.visibleY, height / 2, height);
    this.calc(width, height);
    this.lastElementX = this.prevButton.width;
    this.x = this.prevButton.width;
    this.visibleX = this.prevButton.width;
  }
  calc(width, height) {
    this.width = width - this.prevButton.width - this.nextButton.width;
    this.height = height;
    this.nextButton.x = width - this.nextButton.width;
  }
  add(element) {
    element.x = this.lastElementX + this.gapX;
    element.y = this.visibleY + this.gapY;
    this.elements.push(element);
    this.lastElementX = element.x + element.width;
  }
  isCursorInPrevButton(x, y) {
    return (x > this.prevButton.x) && (x < this.prevButton.x + this.prevButton.width) &&
      (y > this.prevButton.y) && (y < this.prevButton.y + this.prevButton.height);
  }
  isCursorInNextButton(x, y) {
    return (x > this.nextButton.x) && (x < this.nextButton.x + this.nextButton.width) &&
      (y > this.nextButton.y) && (y < this.nextButton.y + this.nextButton.height);
  }
  isCursorInMenu(x, y) {
    return (x > this.visibleX) && (x < this.visibleX + this.width) &&
      (y > this.visibleY) && (y < this.visibleY + this.height);
  }
  getElement(x, y) {
    for(let element of this.elements) {
      if((x > element.x) && (x < element.x + element.width) &&
          (y > element.y) && (y < element.y + element.height)) {
        let newElement = Object.create(element);
        newElement.id = this.idNewElement++;
        return newElement;
      }
    }
  }
  prev() {
    let smallMenu = (2 * this.elements[0].width) > this.width;
    if(this.x < this.prevButton.width - 1) {
      this.elements.forEach(element => {
        if(smallMenu) {
          element.x += (element.width - this.gapX);
        } else {
          element.x += (this.width - element.width - this.gapX);
        }
      });
      this.x = this.elements[0].x - this.gapX;
      this.lastElementX = this.elements[this.elements.length-1].x + this.elements[this.elements.length-1].width;
    }
  }
  next() {
    let smallMenu = (2 * this.elements[0].width) > this.width;
    if(this.lastElementX > this.width + this.prevButton.width) {
      this.elements.forEach(element => {
        if(smallMenu) {
          element.x -= element.width - this.gapX;
        } else {
          element.x -= (this.width - element.width - this.gapX);
        }
      });
      this.x = this.elements[0].x - this.gapX;
      this.lastElementX = this.elements[this.elements.length-1].x + this.elements[this.elements.length-1].width;
    }
  }
  draw() {
    this.elements.forEach(element => {
      element.draw();
    });
    this.drawButtons();
    this.ctx.lineWidth = 4;
    this.ctx.beginPath();
    this.ctx.moveTo(0, this.height);
    this.ctx.lineTo(this.width + this.prevButton.width + this.nextButton.width, this.height);
    this.ctx.stroke();
  }
  drawButtons() {
    this.ctx.fillStyle = '#EfEfEf';
    this.ctx.fillRect(this.prevButton.x, this.prevButton.y, this.prevButton.width, this.prevButton.height);
    this.ctx.fillRect(this.nextButton.x, this.nextButton.y, this.nextButton.width, this.nextButton.height);
    this.ctx.strokeStyle = '#e6e6e9';
    this.ctx.strokeRect(this.prevButton.x, this.prevButton.y, this.prevButton.width, this.prevButton.height);
    this.ctx.strokeRect(this.nextButton.x, this.nextButton.y, this.nextButton.width, this.nextButton.height);

    this.prevImage.draw(this.ctx, 0, 0, this.prevImage.width, this.prevImage.height, this.prevButton.x, this.prevButton.y + this.prevButton.height / 4, this.prevButton.width, this.prevButton.height / 2);
    this.nextImage.draw(this.ctx, 0, 0, this.nextImage.width, this.nextImage.height, this.nextButton.x, this.nextButton.y + this.nextButton.height / 4, this.nextButton.width, this.nextButton.height / 2);
  }
}

class Button {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }
}