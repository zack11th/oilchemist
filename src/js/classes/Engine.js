export default class Engine {
  constructor() {
    this.gameEngine = null;
    // this.mouse = {x: 0, y: 0};
  }
  nextStep(gameLoop) {
    let step = requestAnimationFrame ||
                webkitRequestAnimationFrame ||
                mozRequestAnimationFrame ||
                oRequestAnimationFrame ||
                msRequestAnimationFrame ||
                function (callback) {
                  setTimeout(callback, 1000/60)
                };
    return step(gameLoop);
  }
  engineStart(callback) {
    this.gameEngine = callback;
    this.engineStep();
  }
  engineStep = () => {
    this.gameEngine();
    this.nextStep(this.engineStep);
  }
  setEngine(callback) {
    this.gameEngine = callback;
  }
}