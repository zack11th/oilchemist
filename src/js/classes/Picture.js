export default class Picture {
  constructor() {
    this.dom = null;
    this.width = 0;
    this.height = 0;
    this.loaded = false;
  }
  load(path, width, height) {
    let image = document.createElement('img');
    this.dom = image;
    image.onload = () => {
      this.width = width ? width : image.width;
      this.height = height ? height : image.height;
      this.loaded = true;
    };
    image.src = path;
    return this;
  }
  draw(ctx, startX, startY, width, height, x, y, frameX = this.width, frameY = this.height) {
    if(!this.loaded) return;
    ctx.drawImage(this.dom, startX, startY, width, height, x, y, frameX, frameY);
  }

}