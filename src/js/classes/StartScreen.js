export default class StartScreen {
  constructor(ctx, sizeCanvas) {
    this.ctx = ctx;
    this.showed = true;
    this.width = sizeCanvas.width;
    this.height = sizeCanvas.height;
  }
  show(sizeCanvas, image) {
    if(this.showed){
      if(image.dom) {
        let scale = image.height / sizeCanvas.height;
        let dx = (sizeCanvas.width - (image.width / scale)) / 2;
        this.ctx.drawImage(image.dom, 0, 0, image.width, image.height, dx, 0, image.width / scale, sizeCanvas.height);
      }
    }
  }
  start(x, y) {
    if(x > 0 && x < this.width && y > 0 && y < this.height) {
      this.showed = false;
    }
  }
}