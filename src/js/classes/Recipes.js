const recipes = [
  {
    reagents: ['etilen', 'propilen'],
    result: 'kauchuk'
  },
  {
    reagents: ['aceton', 'fenol'],
    result: 'difenilolpropan'
  },
  {
    reagents: ['bensol', 'metiletilketon'],
    result: 'metiletilketonbensol'
  }
  // {
  //   reagents: ['orange', 'grey'],
  //   result: 'yellow'
  // },
  // {
  //   reagents: ['green', 'grey', 'yellow'],
  //   result: 'orange'
  // }
];

export default class Recipes {
  constructor() {
    this.recipes = {};
  }
  addAllRecipes() {
    for(let recipe of recipes) {
      for(let reagent of recipe.reagents) {
        if(!this.recipes.hasOwnProperty(reagent)) this.recipes[reagent] = [];
        this.recipes[reagent].push(recipe);
      }
    }
  }
  hasRecipe(key, arr) {
    if(this.recipes.hasOwnProperty(key)){
      for(let recipe of this.recipes[key]) {
        let reagents = recipe.reagents.sort();
        if(arr.length !== recipe.reagents.length) continue;
        if(arr.sort().every((val, ind) => val === reagents[ind])) {
          return recipe.result;
        }
      }
    }
    return false;
  }
}