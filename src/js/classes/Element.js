import Picture from './Picture';

export default class Element {
  constructor(ctx, name, image, x = 0, y = 0, width = 197, height = 281,  scale = 1.7) {
    this.ctx = ctx;
    this.name = name;
    this.image = image;
    this.frameX = elementsText[name].frame;
    this.frameY = 1;
    this.descr = elementsText[name].descr;
    this.x = x;
    this.y = y;
    this.scale = scale;
    this.width = width / scale;
    this.height = height / scale;
    this.originW = width;
    this.originH = height;
    this.hitX = 27;
    this.hitY = 25;
    this.hitW = 110 / scale;
    this.hitH = 140 / scale;
  }
  draw() {
    this.ctx.drawImage(this.image.dom, (this.frameX - 1) * this.originW, (this.frameY - 1) * this.originW, this.originW, this.originH, this.x, this.y, this.originW / this.scale, this.originH / this.scale);
  }
  select(sizeCanvas, menuHeight) {
    let height = sizeCanvas.height - menuHeight - 20;
    let scale = this.descr.height / height;
    this.descr.draw(this.ctx, 0, 0, this.descr.width, this.descr.height, 10, menuHeight + 10, this.descr.width / scale, height)
  }
  intersect(element) {
    return !((this.x + this.hitX + this.hitW < element.x + element.hitX) ||
      (this.y + this.hitY + this.hitH < element.y + element.hitY) ||
      (this.x + this.hitX > element.x + element.hitX + element.hitW) ||
      (this.y + this.hitY > element.y + element.hitY + element.hitH));
  }
}



const elementsText = {
  'etilen': {frame: 1, descr: new Picture().load('./img/text/etilen.png'), title: 'Этилен', text: 'Органическое химическое соединение, описываемое формулой С2H4. Бесцветный горючий газ легче воздуха со слабым сладковатым запахом.  Играет чрезвычайно важную роль в промышленности, а также является фитогормоном. Этилен — самое производимое органическое соединение в мире.'},
  'propilen': {frame: 2, descr: new Picture().load('./img/text/propilen.png'), title: 'Пропилен', text: 'Органическое вещество, ненасыщенный углеводород из класса алкенов. При нормальных условиях — бесцветный газ со слабым неприятным запахом. Является важным продуктом промышленного синтеза и исходным сырьём для производства полипропилена и других органических соединений.'},
  'aceton': {frame: 3, descr: new Picture().load('./img/text/aceton.png'), title: 'Ацетон', text: 'Своё название ацетон получил от лат. acetum — уксус. Это связано с тем, что ранее ацетон получали из ацетатов, а из самого ацетона получали синтетическую ледяную уксусную кислоту. Ацетон является ценным промышленным растворителем и благодаря низкой токсичности он получил широкое применение. '},
  'metiletilketon': {frame: 4, descr: new Picture().load('./img/text/metiletilketon.png'), title: 'МЭК', text: 'Это бесцветная подвижная легколетучая жидкость с запахом, напоминающим запах ацетона. Обладает всеми химическими свойствами, характерными для алифатических кетонов, используется как растворитель и сырьё в органическом синтезе.Метилэтилкетон - токсичное вещество. '},
  'fenol': {frame: 5, descr: new Picture().load('./img/text/fenol.png'), title: 'Фенол', text: 'Представляет собой бесцветные игольчатые кристаллы, розовеющие на воздухе из-за окисления, приводящего к образованию окрашенных веществ (это связано с промежуточным образованием хинонов). Обладает специфическим запахом (таким, как запах гуаши, так как в состав гуаши входит фенол). '},
  'bensol': {frame: 6, descr: new Picture().load('./img/text/bensol.png'), title: 'Бензол', text: 'Бензол входит в состав бензина, широко применяется в промышленности, является исходным сырьём для производства лекарств, различных пластмасс, синтетической резины, красителей.Хотя бензол входит в состав сырой нефти, в промышленных масштабах он синтезируется из других её компонентов. '},
  'kauchuk': {frame: 7, descr: new Picture().load('./img/text/kauchuk.png'), title: 'Этилен-пропиленовый каучук', text: 'Это синтетические эластомеры. Применяются в производстве ударопрочного полипропилена, резино-технических изделий, губчатых изделий, для изоляции проводов и кабелей.'},
  'difenilolpropan': {frame: 8, descr: new Picture().load('./img/text/difenilolpropan.png'), title: 'Дифенилол-пропан', text: 'После смешения фенола с ацетоном и добавления катализатора в первое время смесь остается прозрачной и однородной, так как смешанные компоненты взаимно растворяются. Однако по мере протекания реакции жидкость становится густой и менее подвижной.'},
  'metiletilketonbensol': {frame: 9, descr: new Picture().load('./img/text/metiletilketonbensol.png'), title: 'Метилэтил-кетон бензол', text: 'Приведенные данные показывают, что метилизобутилкетон и смесь ацетона с бензолом (толуолом) лучше растворяют легкоплавкие парафины, чем смесь МЭК с бензолом (толуолом), что подтверждается данными.  '}
};