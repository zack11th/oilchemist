import Element from './classes/Element'
import Engine from './classes/Engine'
import Menu from './classes/Menu'
import Recipes from './classes/Recipes'
import Picture from './classes/Picture'
import StartScreen from './classes/StartScreen'

//*********** variables *****************
let ctx;
let engine = new Engine();
let mouse = {
  x: 0,
  y: 0
};
let startScreen;
let menu;
let menuHeight = 170;
let recipes;
let elements = [];
let selected = false;
let intersected = [];
let for_destroy = [];

//********** images objects ***********
let startScreen_img;
let fonRing_img;
let prevButton_img;
let nextButton_img;
let elements_img;

// **************************************
// ************* GAME *******************

export function game(cnv) {
  ctx = cnv.getContext('2d');
  let sizeCanvas = calcCanvas(cnv);

  // initial images
  startScreen_img = new Picture().load('./img/startScreen.jpg');
  fonRing_img = new Picture().load('./img/fonRing.svg');
  prevButton_img = new Picture().load('./img/prevBtn.svg');
  nextButton_img = new Picture().load('./img/nextBtn.svg');
  elements_img = new Picture().load('./img/elements.svg');

  // initial start game
  startScreen = new StartScreen(ctx, sizeCanvas);
  menu = new Menu(ctx, prevButton_img, nextButton_img);
  menu.init(sizeCanvas.width, menuHeight);
  menu.add(new Element(ctx, 'etilen', elements_img));
  menu.add(new Element(ctx, 'propilen', elements_img));
  menu.add(new Element(ctx, 'aceton', elements_img));
  menu.add(new Element(ctx, 'metiletilketon', elements_img));
  menu.add(new Element(ctx, 'fenol', elements_img));
  menu.add(new Element(ctx, 'bensol', elements_img));

  recipes = new Recipes();
  recipes.addAllRecipes();

  // ********** listeners ************
  cnv.onmousemove = move;
  cnv.onmousedown = down;
  cnv.onmouseup = up;

  cnv.addEventListener('touchmove', () => move(event, cnv), false);
  cnv.addEventListener('touchstart', () => down(event, cnv), false);
  cnv.addEventListener('touchend', up, false);

  // *********** GAME LOOP **************
  function gameLoop() {
    ctx.clearRect(0, 0, sizeCanvas.width, sizeCanvas.height);

    if(startScreen.showed) {
      startScreen.show(sizeCanvas, startScreen_img);
    } else {
      fonRing_img.draw(ctx, 0, 0, fonRing_img.width, fonRing_img.height, (sizeCanvas.width - fonRing_img.width), (menuHeight + 20), fonRing_img.width, fonRing_img.height);
      menu.draw();
      elements.forEach(element => {
        element.draw();
      });
      if(selected) {
        selected.select(sizeCanvas, menuHeight);
        selected.draw();
      }
    }
  }

  engine.engineStart(gameLoop);

  // ******** recalc canvas on resize ************
  window.addEventListener('resize', function () {
    let resizeTimeout;
    if(!resizeTimeout) {
      resizeTimeout = setTimeout(function () {
        resizeTimeout = null;
        sizeCanvas = calcCanvas(cnv);
        menu.calc(sizeCanvas.width, menuHeight)
      }, 100);
    }
  });
}
// ************* end game function **************
// *********************************************
// ************* methods ***********************
function isCursorInRect(x, y, item) {
  return (x > item.x) && (x < item.x + item.width) &&
    (y > item.y) && (y < item.y + item.height);
}

function calcMouse(e, cnv) {
  if(cnv){
    mouse.x = e.changedTouches[0].clientX - cnv.getBoundingClientRect().x;
    mouse.y = e.changedTouches[0].clientY - cnv.getBoundingClientRect().y;
  }else{
    mouse.x = e.offsetX;
    mouse.y = e.offsetY;
  }
}

function calcCanvas(cnv) {
  let container = document.querySelector('.oil-container');
  let width = container.clientWidth;
  let height = container.clientHeight;
  cnv.width = width;
  cnv.height = height;
  return {width, height}
}

function destroy(id) {
  let index = elements.findIndex(el => el.id === id);
  if (index !== -1) elements.splice(index, 1);
}

function isIntersect() {
  intersected = [];
  for_destroy = [];
  intersected.push(selected.name);
  elements.forEach(element => {
    if(selected.id !== element.id){
      if(selected.intersect(element)) {
        intersected.push(element.name);
        for_destroy.push(element.id);
      }
    }
  });
  return intersected.length > 1;
}

// ******* events functions ************
function move(e, cnv) {
  calcMouse(e, cnv);
  if(typeof selected === 'object') {
    selected.x = mouse.x - selected.width / 2;
    selected.y = mouse.y - selected.height / 2;
  }
}

function down(e, cnv) {
  e.preventDefault();
  e.stopPropagation();
  calcMouse(e, cnv);
  if(startScreen.showed) {
    startScreen.start(mouse.x, mouse.y);
  }
  if(menu.isCursorInNextButton(mouse.x, mouse.y)) {
    menu.next();
  }
  if(menu.isCursorInPrevButton(mouse.x, mouse.y)) {
    menu.prev();
  }
  if(menu.isCursorInMenu(mouse.x, mouse.y)) {
    let element = menu.getElement(mouse.x, mouse.y);
    if(element){
      if(!selected) {
        elements.push(element);
        selected = element;
      }
    }
  }
  if(!selected) {
    elements.forEach(element => {
      if(isCursorInRect(mouse.x, mouse.y, element)) {
        selected = element;
      }
    });
  }
}

function up() {
  if(selected) {
    if(selected.y + selected.hitY < menu.height){
      destroy(selected.id)
    }else{
      if(isIntersect()) {
        let result = recipes.hasRecipe(selected.name, intersected);
        if(result) {
          if(menu.elements.findIndex(el => el.name === result) === -1) {
            menu.add(new Element(ctx, result, elements_img));
          }
          for_destroy.forEach(id => {
            destroy(id);
          });
          destroy(selected.id);
          let element = new Element(ctx, result, elements_img, selected.x, selected.y);
          element.id = selected.id;
          elements.push(element);
        }
      }
    }
    selected = false;
  }
}